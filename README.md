<h1>TP4 : CréaPro</h1>

<p>Objectif : Refonte du site <a href="http://crea.pro">CréaPro</a></p>

<p>Contraintes :</p>
<ul>
    <li>4h de cours</li>
    <li>Site responsive avec Bootstrap</li>
    <li>Petite charte graphique (logo, couleur, typo...)</li>
</ul>