<?php
	ob_start();
	$url=$_SERVER['REQUEST_URI'];
	$array=['/index', '/projects', '/project-show', '/contact','/404'];
	if ($url==="/")
		$url='/index';
	$template='/default';
	if (in_array($url, $array))
		$view=$url;
	else{
		header("HTTP/1.0 404 Not Found");
		header("Location: /404");
	}

	switch ($url) {
		case "/index":
			$nav = "fixed-top";
			$titre = "";
			break;
		case "/projects":
			$titre = "Mes Projets";
			$nav = "sticky-top";
			break;
		case "/project-show":
			$titre = "Titre du Projet";
			$nav = "sticky-top";
			break;
		case "/404":
			$titre = "Erreur 404";
			$nav = "sticky-top";
			break;
	}

	require (__DIR__.'/view/pages' . $view . '.php');
	$content = ob_get_clean();
	require (__DIR__.'/view/template' . $template . '.php');