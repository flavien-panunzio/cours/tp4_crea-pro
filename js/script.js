$(document).ready(function(){
	//target _blank ----------------------------------------------------------------
		$("a[href^='http://'], a[href^='https://']").attr("target","_blank");

	//SMOOTH SCROLL ----------------------------------------------------------------
		$('a[href^="#"]').click(function(){
			var the_id = $(this).attr("href");
			if (the_id === '#') {
				return;
			}
			$('html, body').animate({
				scrollTop:$(the_id).offset().top
			}, 'slow');
			return false;
		});
});