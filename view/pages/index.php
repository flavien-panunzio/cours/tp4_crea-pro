<main class="container-fluid no-padding index" id="content">
	<article class="row formule no-padding">
		<div class="col-md-7 flex-center">
			<h2>La formule</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam eveniet aspernatur doloremque, praesentium maxime ut suscipit non deserunt id reprehenderit quae dolore alias natus repudiandae. In officiis vero voluptates, aliquam!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga ut nulla dicta, ab doloremque consequatur harum mollitia, ipsam minus magnam quaerat praesentium ratione, amet aperiam incidunt. Iste porro molestiae non.</p>
		</div>
		<div class="col-md-5">
			<img src="/images/landing.jpg">
		</div>
	</article>
	<article class="spec">
		<h2>Un service de qualité</h2>
		<div class="items">
			<div class="item flex-center">
				<div class="rond flex-center">
					<i class="fas fa-chart-line"></i>
				</div>
				<div class="description">
					Lorem ipsum dolor sit amet.
				</div>
			</div>
			<div class="item flex-center">
				<div class="rond flex-center">
					<i class="fas fa-dollar-sign"></i>
				</div>
				<div class="description">
					Lorem ipsum dolor sit amet.
				</div>
			</div>
			<div class="item flex-center">
				<div class="rond flex-center">
					<i class="fas fa-comments"></i>
				</div>
				<div class="description">
					Lorem ipsum dolor sit amet.
				</div>
			</div>
		</div>
	</article>
	<article class="sponso">
		<h2>Ils nous font confiance</h2>
		<div class="container">
			<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/280px-Tux.svg.png" alt="logo linux">
			<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Dell_logo_2016.svg/langfr-280px-Dell_logo_2016.svg.png" alt="logo dell">
			<img src="https://www.balistik.fr/img/m/123-large_default.jpg">
			<img src="https://www.logolynx.com/images/logolynx/d7/d731cb9fc8df9ac4083870f21f6ffde6.png" alt="logo lg">
		</div>
	</article>
</main>