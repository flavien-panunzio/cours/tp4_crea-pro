<main class="container project-show" id="content">
	<h2>Mon projet</h2>
	<div class="row">
		<div class="col-md-6 flex-center">
			<img src="https://unsplash.it/1000/1000?random=">
		</div>
		<div class="col-md-6 flex-center">
			<h3>Détails du projet</h3>
			<table>
				<tr>
					<th>SLUG projet</th>
					<th>monprojet.tp.crea.pro</th>
				</tr>
				<tr>
					<th>Version PHP</th>
					<th>7.2.10</th>
				</tr>
				<tr>
					<th>Version Apache</th>
					<th>2.4.35</th>
				</tr>
				<tr>
					<th>Login</th>
					<th>MonPseudo</th>
				</tr>
				<tr>
					<th>Mot de Passe</th>
					<th>P@$$W0rd</th>
				</tr>
			</table>
			<div>
				<button class="btn btn-danger">Redémarrer le serveur</button>
				<button class="btn btn-primary">Accéder au site</button>
			</div>
		</div>
	</div>
	<div class="outils">
		<h3>Les outils</h3>
		<div>
			<div>
				<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/PhpMyAdmin_logo.png/220px-PhpMyAdmin_logo.png">
				<p>PhpMyAdmin</p>
			</div>
			<div>
				<img src="https://letsencrypt.org/images/le-logo-wide.png">
				<p>Let's Encrypt</p>
			</div>
			<div>
				<img src="https://www.malekal.com/wp-content/uploads/recherche-fichiers-windows-logo.png">
				<p>Explorateur de fichiers</p>
			</div>
		</div>
	</div>
	<div class="cms">
		<h3>Installer un CMS</h3>
		<div>
			<div>
				<img src="https://s.w.org/style/images/about/WordPress-logotype-simplified.png">
				<p>Wordpress</p>
			</div>
			<div>
				<img src="https://cdn.worldvectorlogo.com/logos/joomla.svg">
				<p>Joomla</p>
			</div>
			<div>
				<img src="https://www.drupal.org/files/drupal%208%20logo%20isolated%20CMYK%2072.png">
				<p>Drupal</p>
			</div>
			<div>
				<img src="https://d33np9n32j53g7.cloudfront.net/assets/stacks/liferay/img/liferay-stack-220x234-f4a41615fa027676eeeec1e2f76eb64b.png">
				<p>Liferay</p>
			</div>
		</div>
	</div>
</main>