<main class="container projects" id="content">
	<h2>Tous mes projets</h2>
	<div class="grille">
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
		<div>
			<img src="https://unsplash.it/1000/1000?random=">
			<div>
				<h3>Titre du Projet</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos temporibus et maxime, similique numquam optio autem perspiciatis ad asperiores, rem consectetur in voluptatem deserunt voluptatum natus debitis qui molestiae cum!</p>
				<a class="btn btn-primary" href="/project-show">Voir plus</a>
			</div>
		</div>
	</div>
</main>