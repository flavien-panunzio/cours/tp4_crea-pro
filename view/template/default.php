<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" href="/images/favicon.png" type="image/png">

		<?php if ($_SERVER['REQUEST_URI'] != '/') $titre.=' | ' ?>
		<title><?=$titre?>Créa Pro (V2)</title>

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<link rel="stylesheet" type="text/css" href="/style/css/style.css">
	</head>
	<body>
		<header id="<?php if($_SERVER['REQUEST_URI'] == '/') : echo 'header-landing'; else : echo 'header-default'; endif;?>">
			<?php if ($_SERVER['REQUEST_URI'] == '/') : ?>
				<div class="landing">
					<div></div>
					<h1>Bienvenue sur CréaPro</h1>
					<div>
						<a class="btn btn-primary" href="#content">Découvrir CréaPro</a>
						<a class="btn btn-primary" href="/projects">Mes Projets</a>
					</div>
				</div>
				<div class="svg">
					<a href="#content">
						<svg class="scroll" viewBox="0 0 164 83" version="1.1" xmlns="http://www.w3.org/2000/svg" xml:space="preserve">
							<path class="path" d="M1,1l81,81l81,-81"/>
						</svg>
					</a>
				</div>
			<?php else: ?>
				<div></div>
				<h1><?=$titre?></h1>
			<?php endif; ?>
		</header>
		<nav class="navbar navbar-expand-sm navbar-light <?=$nav?>">
			<a class="navbar-brand" href="/"><img src="/images/logo_noir.png"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav text-center">
					<li class="nav-item">
						<a class="nav-link" href="/">Accueil</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Projets</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="/projects">Tous les projets</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="/project-show">Projet 1</a>
							<a class="dropdown-item" href="/project-show">Projet 2</a>
							<a class="dropdown-item" href="/project-show">Projet 3</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<form class="form">
								<div class="form-group">
									<label for="Email">Email</label>
									<input class="form-control" type="text" id="Email">
								</div>
								<div class="form-group">
									<label for="pswd">Mot de passe</label>
									<input class="form-control" type="password" id="pswd">
								</div>
								<div class="checkbox">
									<label><input type="checkbox"> Rester connecté</label>
								</div>
								<div class="bottom text-center">
									<button class="btn btn-primary">Se connecter</button>
								</div>
							</form>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#contact">Contact</a>
					</li>
				</ul>
			</div>
		</nav>
		
		<?=$content?>
		
		<footer>
			<div class="container" id="contact">
				<h2>Formulaire de contact</h2>
				<form class="flex-center" method="post">
					<div class="form-row no-padding">
						<div class="col-md-6">
							<label for="prenom">Nom</label>
							<input type="text" name="prenom" class="form-control" id="prenom" required>
						</div>
						<div class="col-md-6">
							<label for="nom">Prénom</label>
							<input type="text" name="nom" class="form-control" id="nom" required>
						</div>	
					</div>
					<div class="form-row no-padding">
						<div class="col-md-6">
							<label for="email">E-mail</label>
							<input type="email" name="email" class="form-control" id="email" required>
						</div>
						<div class="col-md-6">
							<label for="tel">Téléphone</label>
							<input type="number" name="tel" class="form-control" id="tel" required>
						</div>
					</div>
					<div class="form-row no-padding">
						<div class="col-md-12">
							<label for="message">Message</label>
							<textarea rows="4" cols="20" name="message" class="form-control" id="message" required></textarea>
						</div>
					</div>
					<button type="submit" class="btn btn-primary submit">Envoyer</button>
				</form>
			</div>
			<div class="bandeau">
				<p>Site créé par <a href="http://flavien-panunzio.tk/">Flavien Panunzio</a> | &copy 2018 Panunzio Flavien</p>
			</div>
		</footer>
		<script src="/include/jquery.min.js"></script>
		<script src="/include/popper.min.js"></script>
		<script src="/include/bootstrap/js/bootstrap.min.js"></script>
		<script src="/js/script.js"></script>
	</body>
</html>